package org.example;

import org.json.JSONObject;

public class UserJson {

    private String username;
    private String password;
    private String email;

    // Constructor
    public UserJson(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public JSONObject createJson(){
        JSONObject userTable = new JSONObject();

        userTable.put("username",username);
        userTable.put("password",password);
        userTable.put("email",email);

        return userTable;
    }

    @Override
    public String toString() {
        return "UserJson{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }






}
