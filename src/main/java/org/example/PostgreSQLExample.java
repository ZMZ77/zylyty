package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PostgreSQLExample {

    public void connection() {
        // Database connection parameters
        String url = "jdbc:postgresql://localhost:5432/zylyty";
        String username = "postgres"; // Replace with your PostgreSQL username
        String password = "Sillym77"; // Replace with your PostgreSQL password

        // Establish database connection
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            // Create and execute SQL query
            String sql = "SELECT * FROM users";

            try (Statement statement = connection.createStatement();
                 ResultSet resultSet = statement.executeQuery(sql)) {
                // Process the results
                while (resultSet.next()) {
                    String username1 = resultSet.getString("username");
                    String password1 = resultSet.getString("password");
                    String email = resultSet.getString("email");

                    // Process retrieved data (e.g., print to console)
                    System.out.println("Username: " + username1 + ", Password: " + password1 + ", Email: " + email);
                }
            }

        } catch (SQLException e) {
            // Handle database connection errors
            e.printStackTrace();
        }
    }



}
