package org.example;
import java.util.Scanner;

import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.client.RestTemplate;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        RestTemplate restTemplate = new RestTemplate();
        // Add MappingJackson2HttpMessageConverter to the RestTemplate's message converters
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        Requests r1 = new Requests(restTemplate, jdbcTemplate);


        System.out.printf("If you want to sign in, press 's'%nIf you want to login, press 'l': ");
        String pick = scanner.nextLine();

        if (pick.equals("s")) {
            System.out.println("Proceeding  with registration..");

            // Get user inputs
            System.out.print("Enter your name: ");
            String name = scanner.nextLine();

            System.out.print("Enter your password (maximum 8 characters): ");
            String password = scanner.nextLine();

            while (password.length() > 8) {
                System.out.println("Password must not exceed 8 characters.");
                System.out.print("Enter your password (maximum 8 characters) ");
                password = scanner.nextLine();
            }

            System.out.print("Enter your email: ");
            String email = scanner.nextLine();

            scanner.close();

            UserJson user = new UserJson(name, password, email);

            JSONObject userJson = user.createJson();


            // Call the registerUser method with the User object
            ResponseEntity<String> response = r1.registerUser(userJson);

            // Print the response
            System.out.println("Response status code: " + response.getStatusCode());
            System.out.println("Response body: " + response.getBody());
            System.out.println(user);



        } else if (pick.equals("l")) {
            System.out.println("Proceeding  with login..");
        } else {
            System.out.println("You can only pick s or l..");
        }


        
        //db connection
      //  PostgreSQLExample postgreSQLExample = new PostgreSQLExample();
      //  postgreSQLExample.connection();

    }
}