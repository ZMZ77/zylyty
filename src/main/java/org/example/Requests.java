package org.example;

import org.json.JSONArray;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.json.JSONObject;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@SpringBootApplication
@RestController
public class Requests {

    private final RestTemplate restTemplate;
    private final JdbcTemplate jdbcTemplate;


    public Requests(RestTemplate restTemplate, JdbcTemplate jdbcTemplate) {
        this.restTemplate = restTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostMapping("/user/register")
    public ResponseEntity<String> registerUser(@RequestBody JSONObject user) {
        String url = "http://localhost:8080/user/register";

        ResponseEntity<String> response = restTemplate.postForEntity(url, user, String.class);

        HttpStatus statusCode = response.getStatusCode();
        String responseBody = response.getBody();

        if (statusCode == HttpStatus.CREATED) {
            return ResponseEntity.status(HttpStatus.CREATED).body("Registration was successful.");
        } else if (statusCode == HttpStatus.BAD_REQUEST) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The provided data was invalid or incomplete.");
        } else if (statusCode == HttpStatus.I_AM_A_TEAPOT) {
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("User is already registered with the provided email or username.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An unexpected error occurred.");
        }
    }

    @PostMapping("/user/login")
    public ResponseEntity<?> loginUser(@RequestBody JSONObject requestBody, HttpServletResponse response) {
        String username = requestBody.getString("username");
        String password = requestBody.getString("password");

        // Query the database to retrieve user information based on username and password
        String query = "SELECT username, email FROM users WHERE username = ? AND password = ?";
        Map<String, Object> userMap = jdbcTemplate.queryForMap(query, username, password);

        if (!userMap.isEmpty()) {
            String userEmail = (String) userMap.get("email");
            User user = new User(username, userEmail);

            String sessionToken = generateSessionToken();
            Cookie sessionCookie = new Cookie("session", sessionToken);
            sessionCookie.setHttpOnly(true);
            sessionCookie.setMaxAge(5000);
            response.addCookie(sessionCookie);

            return ResponseEntity.ok().body(user);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid username or password.");
        }
    }

    private String generateSessionToken() {
        // Generate a random session token here
        return "your_session_token_here";
    }

    // Endpoint to list all categories
    @GetMapping("/categories")
    public ResponseEntity<List<String>> listCategories() {
        List<String> categories = jdbcTemplate.queryForList("SELECT name FROM categories", String.class);
        return ResponseEntity.ok().body(categories);
    }

    // Endpoint to create categories
    @PostMapping("/categories")
    public ResponseEntity<String> createCategories(@RequestBody List<String> categoryNames,
                                                   @RequestHeader(value = "Token") String adminApiKey) {
        if (!isValidAdminApiKey(adminApiKey)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid admin API key.");
        }

        if (categoryNames.isEmpty()) {
            return ResponseEntity.badRequest().body("No categories provided.");
        }

        for (String categoryName : categoryNames) {
            if (categoryExists(categoryName)) {
                return ResponseEntity.badRequest().body("Category '" + categoryName + "' already exists.");
            }
        }

        for (String categoryName : categoryNames) {
            jdbcTemplate.update("INSERT INTO categories (name) VALUES (?)", categoryName);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body("Categories created successfully.");
    }

    private boolean isValidAdminApiKey(String adminApiKey) {
        return true;
    }

    private boolean categoryExists(String categoryName) {
        String query = "SELECT COUNT(*) FROM categories WHERE name = ?";
        int count = jdbcTemplate.queryForObject(query, Integer.class, categoryName);
        return count > 0;
    }

    @PostMapping("/thread")
    public ResponseEntity<String> createThread(@RequestBody JSONObject requestBody, HttpServletResponse response) {
        String category = requestBody.getString("category");
        String title = requestBody.getString("title");
        JSONObject openingPost = requestBody.getJSONObject("openingPost");
        String text = openingPost.getString("text");

        if (category == null || title == null || text == null) {
            return ResponseEntity.badRequest().body("Missing required parameters.");
        }

        if (!categoryExists(category)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Category does not exist.");
        }

        jdbcTemplate.update("INSERT INTO threads (category, title, text) VALUES (?, ?, ?)", category, title, text);

        return ResponseEntity.status(HttpStatus.CREATED).body("Thread created successfully.");
    }

    @GetMapping("/thread")
    public ResponseEntity<?> listThreads(@RequestParam(required = false) List<String> categories,
                                         @RequestParam(defaultValue = "true") boolean newestFirst,
                                         @RequestParam(defaultValue = "0") int page,
                                         @RequestParam(defaultValue = "10") int pageSize) {

        if (categories == null || categories.isEmpty()) {
            return ResponseEntity.badRequest().body("At least one category must be specified.");
        }

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT * FROM threads WHERE category IN (");
        for (int i = 0; i < categories.size(); i++) {
            queryBuilder.append("?");
            if (i < categories.size() - 1) {
                queryBuilder.append(",");
            }
        }
        queryBuilder.append(")");
        if (newestFirst) {
            queryBuilder.append(" ORDER BY created_at DESC");
        } else {
            queryBuilder.append(" ORDER BY created_at ASC");
        }
        queryBuilder.append(" LIMIT ? OFFSET ?");
        String query = queryBuilder.toString();

        List<Map<String, Object>> threadMaps = jdbcTemplate.queryForList(query, categories.toArray(), pageSize, page * pageSize);

        JSONArray threadsArray = new JSONArray();
        for (Map<String, Object> threadMap : threadMaps) {
            JSONObject threadObject = new JSONObject();
            threadObject.put("id", threadMap.get("id"));
            threadObject.put("category", threadMap.get("category"));
            threadObject.put("title", threadMap.get("title"));
            threadObject.put("author", threadMap.get("author")); // Assuming author information is also stored in the database
            threadObject.put("createdAt", threadMap.get("created_at"));
            threadsArray.put(threadObject);
        }

        JSONObject responseBody = new JSONObject();
        responseBody.put("threads", threadsArray);
        return ResponseEntity.ok().body(responseBody.toString());
    }

    @PostMapping("/thread/post")
    public ResponseEntity<String> addThreadPosts(@RequestBody JSONObject requestBody, HttpServletResponse response) {
        int threadId = requestBody.getInt("threadId");
        JSONArray postsArray = requestBody.getJSONArray("posts");

        if (threadId <= 0 || postsArray == null || postsArray.isEmpty()) {
            return ResponseEntity.badRequest().body("Invalid thread ID or empty posts list.");
        }

        if (threadExists(threadId)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Thread does not exist.");
        }

        // Insert the new posts into the database
        for (int i = 0; i < postsArray.length(); i++) {
            JSONObject postObject = postsArray.getJSONObject(i);
            String text = postObject.getString("text");

            // Insert the post into the database
            jdbcTemplate.update("INSERT INTO posts (thread_id, text) VALUES (?, ?)", threadId, text);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body("Posts added successfully.");
    }

    @GetMapping("/thread/post")
    public ResponseEntity<?> getThreadPosts(@RequestParam("thread_id") int threadId) {
        if (threadId <= 0) {
            return ResponseEntity.badRequest().body("Invalid thread ID.");
        }

        if (threadExists(threadId)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Thread does not exist.");
        }

        List<Map<String, Object>> postMaps = jdbcTemplate.queryForList("SELECT * FROM posts WHERE thread_id = ?", threadId);

        JSONArray postsArray = new JSONArray();
        for (Map<String, Object> postMap : postMaps) {
            JSONObject postObject = new JSONObject();
            postObject.put("author", postMap.get("author")); // Assuming author information is stored in the database
            postObject.put("text", postMap.get("text"));
            postObject.put("createdAt", postMap.get("created_at"));
            postsArray.put(postObject);
        }

        Map<String, Object> threadMap = jdbcTemplate.queryForMap("SELECT * FROM threads WHERE id = ?", threadId);

        JSONObject responseBody = new JSONObject();
        responseBody.put("id", threadMap.get("id"));
        responseBody.put("category", threadMap.get("category"));
        responseBody.put("title", threadMap.get("title"));
        responseBody.put("text", threadMap.get("text"));
        responseBody.put("author", threadMap.get("author")); // Assuming author information is stored in the database
        responseBody.put("createdAt", threadMap.get("created_at"));
        responseBody.put("posts", postsArray);

        return ResponseEntity.ok().body(responseBody.toString());
    }

    private boolean threadExists(int threadId) {
        String query = "SELECT COUNT(*) FROM threads WHERE id = ?";
        int count = jdbcTemplate.queryForObject(query, Integer.class, threadId);
        return count <= 0;
    }

    @GetMapping("/search")
    public ResponseEntity<Map<Integer, List<String>>> search(@RequestParam String text) {
        Map<Integer, List<String>> searchResults = new HashMap<>();

        String query = "SELECT id, title, content FROM threads WHERE LOWER(title) LIKE ? OR LOWER(content) LIKE ?";
        List<Map<String, Object>> threadMatches = jdbcTemplate.queryForList(query, "%" + text.toLowerCase() + "%");

        for (Map<String, Object> threadMatch : threadMatches) {
            int threadId = (int) threadMatch.get("id");
            String title = (String) threadMatch.get("title");
            String content = (String) threadMatch.get("content");

            List<String> snippets = extractSnippets(content, text);

            searchResults.put(threadId, snippets);
        }

        if (searchResults.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(searchResults);
    }

    private List<String> extractSnippets(String content, String searchText) {
        List<String> snippets = new ArrayList<>();
        String[] words = content.split("\\s+");

        for (int i = 0; i < words.length; i++) {
            if (words[i].toLowerCase().contains(searchText.toLowerCase())) {
                int startIndex = Math.max(0, i - 3);
                int endIndex = Math.min(words.length, i + 4);
                snippets.add("..." + String.join(" ", Arrays.copyOfRange(words, startIndex, endIndex)) + "...");
            }
        }

        return snippets;
    }

    @DeleteMapping("/categories")
    public ResponseEntity<String> deleteCategory(@RequestParam String category,
                                                 @RequestHeader(value = "Token") String adminApiKey) {
        if (!isValidAdminApiKey(adminApiKey)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Invalid admin API key.");
        }

        if (category.equalsIgnoreCase("Default")) {
            return ResponseEntity.badRequest().body("The 'Default' category cannot be deleted.");
        }

        if (!categoryExists(category)) {
            return ResponseEntity.badRequest().body("Category '" + category + "' does not exist.");
        }

        jdbcTemplate.update("DELETE FROM threads WHERE category = ?", category);
        jdbcTemplate.update("DELETE FROM categories WHERE name = ?", category);

        return ResponseEntity.ok().body("Category '" + category + "' has been successfully deleted.");
    }











    }












